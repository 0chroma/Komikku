from dataclasses import dataclass
from typing import Optional, List

from pure_protobuf.dataclasses_ import field, message
from pure_protobuf.types import int32, int64
# Created from https://github.com/jobobby04/TachiyomiSY/blob/22933da5d8a2e803a4df8c01aaee6a3e6aa01ecf/app/src/main/java/eu/kanade/tachiyomi/data/backup/full/models/BackupManga.kt

@message
@dataclass
class BackupChapter:
    url: str = field(1)
    name: str = field(2)
    scanlator: Optional[str] = field(3, default=None)
    read: bool = field(4, default=False)
    bookmark: bool = field(5, default=False)
    lastPageRead: int32 = field(6, default=0)
    dateFetch: int64 = field(7, default=0)
    dateUpload: int64 = field(8, default=0)
    chapterNumber: float = field(9, default=0)
    sourceOrder: int32 = field(10, default=0)

@message
@dataclass
class BackupTracking:
    syncId: int32 = field(1)
    libraryId: int64 = field(2)
    mediaId: int32 = field(3, default=0)
    trackingUrl: str = field(4, default="")
    title: str = field(5, default="")
    lastChapterRead: float = field(6, default=0)
    totalChapters: int32 = field(7, default=0)
    score: float = field(8, default=0)
    status: int32 = field(9, default=0)
    startedReadingDate: int64 = field(10, default=0)
    finishedReadingDate: int64 = field(11, default=0)

@message
@dataclass
class BackupHistory:
    url: str = field(1)
    lastRead: int64 = field(2)

@message
@dataclass
class BackupManga:
    source: int64 = field(1)
    url: str = field(2)
    title: str = field(3, default="")
    artist: Optional[str] = field(4, default=None)
    author: Optional[str] = field(5, default=None)
    description: Optional[str] = field(6, default=None)
    genre: List[str] = field(7, default_factory=list)
    status: int64 = field(8, default=0)
    thumbnailUrl: Optional[str] = field(9, default=None)
    dateAdded: int64 = field(13, default=0)
    viewer: int32 = field(14, default=0)
    chapters: List[BackupChapter] = field(16, default_factory=list)
    categories: List[int32] = field(17, default_factory=list)
    tracking: List[BackupTracking] = field(18, default_factory=list)
    favorite: bool = field(100, default=True)
    chapterFlags: int32 = field(101, default=0);
    history: List[BackupHistory] = field(102, default_factory=list)

@message
@dataclass
class BackupCategory:
    name: str = field(1)
    order: int32 = field(2, default=0)
    flags: int32 = field(100, default=0)

@message
@dataclass
class BackupSource:
    name: str = field(1)
    sourceId: int64 = field(2)

@message
@dataclass
class Backup:
    backupManga: List[BackupManga] = field(1)
    backupCategories: List[BackupCategory] = field(2, default_factory=list)
    backupSources: List[BackupSource] = field(101, default_factory=list)
