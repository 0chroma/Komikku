from komikku.models import Manga, Settings, Category
import komikku.models.tachiyomi as tachiyomi_backup
from komikku.utils import log_error_traceback
from komikku.servers.utils import get_allowed_servers_list
from gi.repository import GLib
import gzip
import time
import threading
import logging

logger = logging.getLogger('komikku.models.importer')

class Importer():
    def noop(manga=None):
        pass

    @staticmethod
    def tachiyomi(file_path, on_change=noop, on_complete=noop):
        backup = None
        try:
            with gzip.open(file_path, "rb") as file:
                backup = tachiyomi_backup.Backup.load(file)
        except (UnicodeDecodeError,
                FileNotFoundError,
                gzip.BadGzipFile) as e:
            log_error_traceback(e)
        else:
            # Map Servers to Tachiyomi Sources
            servers_map = {}
            for server in get_allowed_servers_list(Settings.get_default()):
                for sid in server['import_ids'].tachiyomi:
                    servers_map[sid] = server

            # Warn if there's missing sources
            for source in backup.backupSources:
                if source.sourceId not in servers_map:
                    logger.warning('Server %s with id %s missing from available servers',
                                   source.name, source.sourceId)

            # Map Categories, if they don't exist create them
            cat_map = dict()
            # handle existing categories
            existing_cats = Category.list()
            for cat in backup.backupCategories:
                existing_cat = list(filter(lambda c: c.label == cat.name, existing_cats))
                if len(existing_cat) > 0:
                    cat_map[cat.order] = existing_cat[0]
                else:
                    cat_map[cat.order] = Category.new(cat.name)

            # run the import in a thread
            def run_import(backup, servers_map, on_change):
                for manga in backup.backupManga:
                    if not manga.source in servers_map:
                        continue

                    server = servers_map[manga.source]
                    server_class = getattr(server['module'], server['class_name'])()
                    
                    init_manga_data = dict(
                        name=manga.title, 
                        server_id=server['id'],
                        url=None,
                        slug=None,
                        cover=None,
                        chapters=[],
                    )
                    # try getting from URL
                    # can fail if server doesn't implement this function
                    manga_data = init_manga_data
                    manga_data |= server_class.get_manga_initial_data_from_url(server_class.base_url+manga.url)
                    try:
                        manga_data = server_class.get_manga_data(manga_data)
                    except Exception as e:
                        logger.exception('"%s" from %s caused an error while importing:',
                                         manga.title, server_class.name)

                    if manga_data is None:
                        # if the server is implemented correctly this shouldn't happen,
                        # but as a failover try getting via search instead
                        results = None
                        try:
                            results = server_class.search(manga.title)
                        except Exception as e:
                            # some servers require extra args on search
                            # this shouldn't be a requirement but without this info it's hard to track down
                            logger.exception('"%s" from %s caused an error while importing:',
                                             manga.title, server_class.name)
                            continue
                        if results:
                            manga_data = init_manga_data | results[0]
                            manga_data = server_class.get_manga_data(manga_data)
                        else:
                            logger.warning('"%s" from %s failed to import', manga.title, server_class.name)
                            continue
                    
                    try:
                        created_manga = Manga.new(
                            manga_data,
                            server_class,
                            Settings.get_default().long_strip_detection
                        )
                    except Exception as e:
                        logger.exception('"%s" from %s caused an error while importing:',
                                         manga.title, server_class.name)
                        continue

                    # Set Categories
                    for cat in manga.categories:
                        created_cat = cat_map[cat]
                        created_manga.toggle_category(created_cat.id, True) 

                    # set chapter read status
                    for chapter in manga.chapters:
                        for created_chapter in created_manga.chapters:
                            if created_chapter.slug in chapter.url or created_chapter.url is chapter.url:
                                if chapter.lastPageRead > 1:
                                    created_chapter.update(dict(
                                        last_page_read_index=chapter.lastPageRead,
                                        read=1 if chapter.read else 0
                                    ))

                    # notify UI that manga was added
                    GLib.idle_add(on_change, created_manga)

                GLib.idle_add(on_complete)


            thread = threading.Thread(target=run_import, args=(backup, servers_map, on_change))
            thread.daemon=True
            thread.start()

